﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ReadKey();

            while (true)
            {
                
                Thread.Sleep(1000);
                KeyHelper.Send_Key(ConsoleKey.A);
            }
        }
    }

    public static class KeyHelper
    {
        //[DllImport("user32.dll", SetLastError = true)]
        //static extern UInt32 SendInput(UInt32 nInputs, [MarshalAs(UnmanagedType.LPArray, SizeConst = 1)] INPUT[] pInputs, Int32 cbSize);

        [DllImport("user32.dll", SetLastError = true)]
        static extern uint SendInput(uint nInputs, INPUT[] pInputs, int cbSize);

        //[StructLayout(LayoutKind.Sequential)]
        //struct KEYBDINPUT
        //{
        //    public short wScan; public int dwFlags;
        //    public int time; public IntPtr dwExtraInfo;
        //}

        //[StructLayout(LayoutKind.Explicit)]
        //struct INPUT
        //{
        //    [FieldOffset(0)]
        //    public int type;
        //    [FieldOffset(8)]
        //    public KEYBDINPUT ki; //x64 - 8, x32 - 4
        //}

        const int KEYEVENTF_DOWN = 0; //key UP
        //const int KEYEVENTF_EXTENDEDKEY = 0x0001;
        //const int KEYEVENTF_KEYUP = 0x0002; //key UP
        //const int KEYEVENTF_UNICODE = 0x0004;
        //const int KEYEVENTF_SCANCODE = 0x0008; // scancode


        const int INPUT_MOUSE = 0;
        const int INPUT_KEYBOARD = 1;
        const int INPUT_HARDWARE = 2;
        const uint KEYEVENTF_EXTENDEDKEY = 0x0001;
        const uint KEYEVENTF_KEYUP = 0x0002;
        const uint KEYEVENTF_UNICODE = 0x0004;
        const uint KEYEVENTF_SCANCODE = 0x0008;

        struct INPUT
        {
            public int type;
            public InputUnion u;
        }

        [StructLayout(LayoutKind.Explicit)]
        struct InputUnion
        {
            [FieldOffset(0)]
            public MOUSEINPUT mi;
            [FieldOffset(0)]
            public KEYBDINPUT ki;
            [FieldOffset(0)]
            public HARDWAREINPUT hi;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct MOUSEINPUT
        {
            public int dx;
            public int dy;
            public uint mouseData;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct KEYBDINPUT
        {
            /*Virtual Key code.  Must be from 1-254.  If the dwFlags member specifies KEYEVENTF_UNICODE, wVk must be 0.*/
            public ushort wVk;
            /*A hardware scan code for the key. If dwFlags specifies KEYEVENTF_UNICODE, wScan specifies a Unicode character which is to be sent to the foreground application.*/
            public ushort wScan;
            /*Specifies various aspects of a keystroke.  See the KEYEVENTF_ constants for more information.*/
            public uint dwFlags;
            /*The time stamp for the event, in milliseconds. If this parameter is zero, the system will provide its own time stamp.*/
            public uint time;
            /*An additional value associated with the keystroke. Use the GetMessageExtraInfo function to obtain this information.*/
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct HARDWAREINPUT
        {
            public uint uMsg;
            public ushort wParamL;
            public ushort wParamH;
        }

        public static void Send_Key(ConsoleKey keyCode)
        {
            INPUT[] InputData = new INPUT[1];
            InputData[0].type = 1;
            InputData[0].u.ki.wScan = (ushort)keyCode;
            InputData[0].u.ki.dwFlags = KEYEVENTF_DOWN;
            InputData[0].u.ki.time = 0;
            InputData[0].u.ki.dwExtraInfo = IntPtr.Zero;

            uint intReturn = SendInput(1, InputData, Marshal.SizeOf(typeof(INPUT)));

            if (intReturn == 0) //!=1
            {
                int i = Marshal.GetLastWin32Error();
                throw new Exception("Could not send keyDOWN: " + keyCode + ". GLE: " + i);
            }

            INPUT[] InputData2 = new INPUT[1];
            InputData2[0].type = 1;
            InputData2[0].u.ki.wScan = (ushort)keyCode;
            InputData2[0].u.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP | KEYEVENTF_SCANCODE;
            InputData2[0].u.ki.time = 0;
            InputData2[0].u.ki.dwExtraInfo = IntPtr.Zero;

            uint intReturn2 = SendInput(1, InputData2, Marshal.SizeOf(typeof(INPUT)));

            if (intReturn2 == 0) //!=1
            {
                int i = Marshal.GetLastWin32Error();
                throw new Exception("Could not send keyUP: " + keyCode + ". GLE: " + i);
            }
        }
    }
}
