﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

PgUp::
SoundPlay *-1
sleep, 1000
Send, My First Script
sleep, 1000
SoundPlay *-1
return

PgDn::
SoundPlay *16
Send, ^f
return

b::
SoundPlay *16