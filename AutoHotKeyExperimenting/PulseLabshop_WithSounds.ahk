﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

PgUp::
Send, {F6}
SoundPlay *16
sleep, 100
SoundPlay *16
Send, {F7}
return

PgDn::
Send, {F5}
SoundPlay *16
sleep, 100
SoundPlay *16
;Send, !1
;49 in hexa - as LabShop igoners 1 from numpad
Send !{vk31}
return

Esc::
b::
SoundPlay *-1
sleep, 100
SoundPlay *-1
sleep, 100
SoundPlay *-1
sleep, 100
SoundPlay *16
sleep, 100
SoundPlay *16
sleep, 100
SoundPlay *16

ExitApp  ; Exit script with Escape key
return